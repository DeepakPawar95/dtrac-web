<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 01/03/17
 * Time: 11:43 AM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();

$response = array();
$response['errors'] = array();

if(!isset($_POST['name']) || empty($_POST['name']) || !isset($_POST['email']) || empty($_POST['email']) || !isset($_POST['pwd']) || empty($_POST['pwd']) || !isset($_POST['confirmpwd']) || empty($_POST['confirmpwd']) ){
    $response['code'] = 0;
    $response['errors']['message'] = 'No data input.';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}

$name = $_POST['name'];
$email = $_POST['email'];
$pwd = $_POST['pwd'];
$confirmpwd = $_POST['confirmpwd'];


if ($pwd !== $confirmpwd) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Password and confirm password does not match';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}

if (strlen($pwd) < 6) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Password length is less than 6.';
    $response['errors']['code'] = 3;

    echo json_encode($response);
    exit(0);
}


$query = 'INSERT INTO restaurant(name, email, pwd) VALUES(:name, :email, :pwd)';
$database->insertQuery($query, array(':name' => $name, ':email' => $email, ':pwd' => $pwd));
$res = $database->getResponse();

if($res['sql_res'] == false){
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in inserting';
    $response['errors']['code'] = 4;
}else{
    $response['code'] = 1;
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}

echo json_encode($response);