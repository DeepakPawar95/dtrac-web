<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 02/03/17
 * Time: 6:30 PM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();


$response = array();
$response['errors'] = array();


if (!isset($_GET)) {
    $response['code'] = 0;
    $response['errors']['message'] = 'No data input';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}

if (!isset($_SESSION['user']['id']) || empty($_SESSION['user']['id'])) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Not logged in';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}


$uid = $_SESSION['user']['id'];

$query = 'SELECT oid, info, toLocation FROM orders WHERE createdBy = :id';
$database->selectQuery($query, array('id' => $uid));
$res = $database->getResponse();


if ($res['sql_res'] == false) {
    $response['code'] = 0;
    $response['errors']['message'] = 'Error getting data';
    $response['errors']['code'] = 3;
} else {
    $response['code'] = 1;
    $response['data'] = $res['sql_data'];
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}

echo json_encode($response);