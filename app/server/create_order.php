<?php
/**
 * Created by IntelliJ IDEA.
 * User: deepak
 * Date: 01/03/17
 * Time: 2:44 PM
 */

ob_start();
session_start();

require_once($_SERVER['DOCUMENT_ROOT'] . '/server/class/Database.php');

$database = new Database();

$response = array();
$response['errors'] = array();

if(!isset($_POST['info']) || empty($_POST['info']) || !isset($_POST['location']) || empty($_POST['location'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'No data input.';
    $response['errors']['code'] = 1;

    echo json_encode($response);
    exit(0);
}

if(!isset($_SESSION['user']['id']) || empty($_SESSION['user']['id'])){
    $response['code'] = 0;
    $response['errors']['message'] = 'Not logged in';
    $response['errors']['code'] = 2;

    echo json_encode($response);
    exit(0);
}


$info = $_POST['info'];
$location = $_POST['location'];
$uid = $_SESSION['user']['id'];
$pending = 1;


$query = 'INSERT INTO orders(info, toLocation, createdBy, pending) VALUES(:info, :location, :createdBy, :pending)';
$database->insertQuery($query, array(':info' => $info, ':location' => $location, ':createdBy' => $uid, ':pending' => $pending));
$res = $database->getResponse();


if($res['sql_res'] == false){
    $response['code'] = 0;
    $response['errors']['message'] = 'Error in inserting';
    $response['errors']['code'] = 3;
}else{
    $response['code'] = 1;
    $response['errors']['insertId'] = $res['insert_id'];
    $response['errors']['message'] = 'SUCCESS';
    $response['errors']['code'] = -1;
}

echo json_encode($response);