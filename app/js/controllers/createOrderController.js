/**
 * Created by deepak on 01/03/17.
 */

app.controller('createOrderController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {

    $scope.order = {
        info: '',
        location: ''
    };

    $scope.showOrder = false;
    
    $scope.createOrder = function () {
        $scope.orderError = '';
        $scope.orderId = '';
        $scope.orderInfo = '';
        $scope.orderLocation = '';

        console.log("Submitting data", $scope.order);
        
        $http({
            url: 'server/create_order.php',
            method: 'POST',
            data: $.param($scope.order),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Create Order Response", data);

                if (data.code == 1) {
                    console.info('Order created ', $scope.order);
                    $scope.orderError = '';

                    $scope.orderId = data.errors.insertId;
                    $scope.orderInfo = $scope.order.info;
                    $scope.orderLocation = $scope.order.location;

                    $scope.showOrder = true;

                    $scope.order = {
                        info: '',
                        location: ''
                    };
                }
                else {
                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.orderError = 'Please fill the complete form.';
                        }break;
                        case 2:
                        {
                            $scope.orderError = 'Please login again to and retry.';
                            $scope.order = {
                                info: '',
                                location: ''
                            };
                        }
                            break;
                        default :
                        {
                            $scope.orderError = 'Sorry, An unknown error occurred. Please retry!';
                        }
                    }
                }
            })
            .error(function () {
                console.log('Create order error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.orderError = 'Sorry, An unknown error occurred.';
            });
    };

}]);