/**
 * Created by deepak on 02/03/17.
 */

app.controller('ordersController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
    
    /*$scope.orders = {
        id: '',
        info: '',
        location: ''
    };*/
    
    $scope.myOrders = [];
    
    $http({
        url: 'server/orders_data.php',
        method: 'GET',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
        .success(function (data) {
            console.log("Orders response", data);

            $scope.myOrders = [];

            if (data.code == 1) {
                $scope.myOrders = data.data;
            }
        })
        .error(function (data, status, headers, config) {
            console.log('Error', data, 'status', status, 'headers', headers, 'config', config);
        });
    
}]);
