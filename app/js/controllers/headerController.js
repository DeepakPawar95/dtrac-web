/**
 * Created by deepak on 01/03/17.
 */

app.controller('headerController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {

    $(".button-collapse").sideNav();
    
    $http.get('server/check_login.php')
        .success(function (data) {

            console.log("[#SERVER]Check Login", data);
            if(data.status == 1){
                $rootScope.setLogin(data);
            }else{
                $rootScope.unsetLogin();
                $location.path('/login');
            }
        })
        .error(function(data, status, headers, config){
            console.log("[#Header-checkLogin] Login error", data,"status", status,"headers", headers,"config", config);
        });


    $scope.logout = function () {
        $http({
            url: 'server/logout.php'
        })
            .success(function (data) {
                console.log("Logged out user!");
                $rootScope.unsetLogin();
                $location.path('/login');
            })
            .error(function (data, status, headers, config) {
                console.log("Logout error", data, "status", status, "headers", headers, "config", config);
            });

    };

}]);