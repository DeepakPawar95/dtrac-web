/**
 * Created by deepak on 01/03/17.
 */

app.controller('registerController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {

    $scope.user = {
        name: '',
        email: '',
        pwd: '',
        confirmpwd: ''
    };

    $scope.register = function () {
        $scope.registerMsg = '';

        console.log("Submitting data", $scope.user);

        if ($scope.user.pwd !== $scope.user.confirmpwd) {
            $scope.registerMsg = 'Password and confirm password does not match.';
            return;
        }
        if ($scope.user.pwd.length < 6) {
            $scope.registerMsg = 'Passwords must be at least 6 characters long.';
            return;
        }

        $http({
            url: 'server/register.php',
            method: 'POST',
            data: $.param($scope.user),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Register Response", data);

                if (data.code == 1) {
                    //registration complete

                    console.info('going to login');
                    $location.path('/login');

                    $scope.user = {
                        name: '',
                        email: '',
                        pwd: '',
                        confirmpwd: ''
                    };
                }
                else {
                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.registerMsg = 'Please fill the complete form.';
                        }break;
                        case 2:
                        {
                            $scope.registerMsg = 'Password and confirm password does not match.';
                            $scope.user = {
                                name: '',
                                email: '',
                                pwd: '',
                                confirmpwd: ''
                            };
                        }
                            break;
                        case 3:
                        {
                            $scope.registerMsg = 'Password length is less than 6.';
                        }
                            break;
                        default :
                        {
                            $scope.registerMsg = 'Sorry, An unknown error occurred. Please retry!';
                        }
                    }
                }
            })
            .error(function (data, status, headers, config) {
                console.log('Register error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.registerMsg = 'Sorry, An unknown error occurred.';
            });
    };

}]);