/**
 * Created by deepak on 01/03/17.
 */

app.controller('loginController', ['$scope', '$rootScope', '$http', '$location', function ($scope, $rootScope, $http, $location) {

    $scope.user = {
        email: '',
        password: ''
    };

    $scope.login = function () {
        $scope.loginMsg = ''; //clearing the error message on new data submission

        console.log("Submitting data", $scope.user);

        $http({
            url: 'server/login.php',
            method: 'POST',
            data: $.param($scope.user),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function (data) {
                console.log("Login Response", data);

                if (data.code == 1) {
                    //login complete
                    $rootScope.setLogin(data);

                    console.info('going to dashboard');
                    $location.path('/dashboard');

                    $scope.user = {
                        email: '',
                        password: ''
                    };
                }
                else {

                    switch (data.errors.code) {
                        case 1:
                        {
                            $scope.loginMsg = 'Provide inputs.';
                        }
                            break;
                        case 2:
                        {
                            $scope.loginMsg = 'Invalid Email Address or Password.';
                            $scope.user.password = '';
                        }
                            break;
                        default :
                        {
                            $scope.loginMsg = 'Sorry, An unknown error occurred.';
                        }
                    }
                }
            })
            .error(function (data, status, headers, config) {
                console.log('Login error', data, 'status', status, 'headers', headers, 'config', config);
                $scope.loginMsg = 'Sorry, An unknown error occurred.';
            });
    };

}]);
