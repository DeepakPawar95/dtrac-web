/**
 * Created by deepak on 02/03/17.
 */

/* Directives */

app.directive('headerCustom', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/header.html',
        controller: 'MyCtrl'
    };
});

app.directive('headerDashboard', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/directives/header_dashboard.html',
        controller: 'headerController'
    };
});
