'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'myApp.version'
]);


app.run(['$rootScope', function($rootScope) {

    $rootScope.setLogin = function (data) {

        $rootScope.user = {
            id: data.id,
            email: data.email,
            status: data.status
        };

    };

    $rootScope.unsetLogin = function () {
        $rootScope.user = {
            id: '',
            email: '',
            password: '',
            status: 0
        };
    };

}]);



/* Controllers */

app.controller('MyCtrl', [function() {
  angular.element(document).ready(function () {
    $(".button-collapse").sideNav();
  });
}]);