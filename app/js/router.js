/**
 * Created by deepak on 02/03/17.
 */

/* Routers */

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'loginController'
        })
        .when('/register', {
            templateUrl: 'views/register.html',
            controller: 'registerController'
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard.html'
        })
        .when('/create', {
            templateUrl: 'views/create_order.html',
            controller: 'createOrderController'
        })
        .when('/my-orders',{
            templateUrl: 'views/my_orders.html',
            controller: 'ordersController'
        })
        .when('/pay-agent',{
            templateUrl: 'views/pay_agent.html'
        })
        .otherwise({redirectTo: '/'});
}]);
